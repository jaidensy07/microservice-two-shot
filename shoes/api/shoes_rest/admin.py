from django.contrib import admin
from .models import Shoes, BinVO

# Register your models here.

class BinVOAdmin(admin.ModelAdmin):
    pass

class ShoesAdmin(admin.ModelAdmin):
    pass

admin.site.register(BinVO, BinVOAdmin)
admin.site.register(Shoes, ShoesAdmin)