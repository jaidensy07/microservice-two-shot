from django.db import models


# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.CharField(max_length=100)
    bin_size = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"

class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    image_url = models.URLField(max_length=500, null=True)
    
    wardrobe_bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.model_name



