# Wardrobify

Team:

* Jordan Frerichs - Shoes Microservice
* Jaiden Sy - Hats Microservice

## Design

## Shoes microservice
Create a shoe, view the shoe, look at its bin and where it is located. Color, material, etc are viewable in the list view.

## Hats microservice

Create a hat, view the hat, look at its location ie shelf, closet etc. this also provides the color and the style, as well as the materials used for the hat.
