import React from 'react';

class ShoesForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturer: '',
      model_name: '',
      color: '',
      image_url: '',
      wardrobe_bin: '',
      bins: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
    this.handleChangeModelName = this.handleChangeModelName.bind(this);
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangeImageURL = this.handleChangeImageURL.bind(this);
    this.handleChangeWardrobeBin = this.handleChangeWardrobeBin.bind(this);
   
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/bins/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ bins: data.bins });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.bins;

    const shoesUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
      const newShoes = await response.json();
      this.setState({
          manufacturer: '',
          model_name: '',
          color: '',
          image_url: '',
          bins: [],
      });
      
    }
  }

  handleChangeManufacturer(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }

  handleChangeModelName(event) {
    const value = event.target.value;
    this.setState({ model_name: value });
  }

  handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handleChangeImageURL(event) {
    const value = event.target.value;
    this.setState({ image_url: value });
  }

  handleChangeWardrobeBin(event) {
    const value = event.target.value;
    this.setState({ wardrobe_bin: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Pair of Shoes</h1>
            <form onSubmit={this.handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeManufacturer} value={this.state.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeModelName} value={this.state.model_name} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeColor} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
                <label htmlFor="image_url">Image URL</label>
                <textarea onChange={this.handleChangeImageURL} value={this.state.image_url} className="form-control" id="image_url" rows="3" name="image_url"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeWardrobeBin} required name="wardrobe_bin" id="wardrobe_bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {this.state.bins.map(wardrobe_bin => {
                    return (
                      <option key={wardrobe_bin.href} value={wardrobe_bin.href}>{wardrobe_bin.closet_name} Bin: {wardrobe_bin.bin_number}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Add</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoesForm;