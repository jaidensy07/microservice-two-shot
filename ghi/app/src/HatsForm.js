import React from 'react';

class HatsForm extends React.Component {
    
    constructor(props) {
        super(props)
        this.state = {
            fabric: '',
            style_name: '',
            color: '',
            picture_url: '',
            locations: [],
        };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations
        console.log(data);

        const HatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                },
        };
        
        const response = await fetch(HatsUrl, fetchConfig);
        
        if (response.ok) {
            const newHat = await response.json();
            
        
            const cleared = {
              fabric: '',
              style_name: '',
              color: '',
              picture_url: '',
              locations: [],
            };

            this.setState(cleared);
          }
        }

    handleFabricChange(event) {
      const value = event.target.value;
      this.setState({fabric: value})
      }

    handleStyleChange(event) {
      const value = event.target.value;
      this.setState({style_name: value})
      }

    handleColorChange(event) {
      const value = event.target.value;
      this.setState({color: value})
      }

    handlePictureChange(event) {
      const value = event.target.value;
      this.setState({picture_url: value})
      }

    handleLocationChange(event) {
      const value = event.target.value;
      this.setState({location: value})
      }

    

    async componentDidMount() {
        const url = "http://localhost:8100/api/locations/";
    
        const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
            }
        }

  render() {
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Hat</h1>
            <form onSubmit={this.handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
              <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="fabric" required
                type="text" name="fabric" id="fabric"
                className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStyleChange} value={this.state.style_name} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/>
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
                <label htmlFor="picture_url" className ="form-label">Picture Url</label>
                <input onChange={this.handlePictureChange} value={this.state.picture_url}  name="picture_url" required type="url" id="picture_url" className="form-control" rows="3"></input>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} value={this.state.location} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                        return (
                        <option key={location.href} value={location.href}>
                            {location.closet_name} - {location.section_number}/{location.shelf_number}
                        </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default HatsForm;
