import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatsForm from './HatsForm';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesForm from './ShoesForm';
import ShoesList from './ShoesList';
import HatsList from './HatsList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesList />} />
          <Route path="/shoes/new" element={<ShoesForm />} />
          <Route path="hats">
            <Route path=""element={<HatsList />} />
          </Route>
          <Route path="hats">
            <Route path="new"element={<HatsForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
