import React from 'react';
import { Link } from 'react-router-dom';


const deleteShoe = async (id) => {
    fetch(`http://localhost:8080/api/shoes/${id}`, {
        method: 'delete',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    window.location.reload();
}

function ShoesColumn(props) {
  return (
    <div className="col">
      {props.list.map(shoe => {
        return (
          <div key={shoe.id} className="card mb-3 shadow">
            <img src={ !shoe.image_url ? 'https://artincontext.org/wp-content/uploads/2022/01/shoe-drawing-08.jpg' : shoe.image_url } className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{shoe.manufacturer} {shoe.model_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {shoe.color}
              </h6>
            </div>
            <div className="card-footer">
              {shoe.wardrobe_bin.closet_name} - bin: {shoe.wardrobe_bin.bin_number}
            </div>
            <button onClick={() => deleteShoe(shoe.id)} type="delete-button" class="btn btn-outline-danger">Remove these shoes</button>
          </div>
        );
      })}
    </div>
  );
}


class ShoesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          shoesColumns: [[], [], []],
        };
      }
  
    async componentDidMount() {
      const url = 'http://localhost:8080/api/shoes/';
  
      try {
        const response = await fetch(url);
        if (response.ok) {
          // Get the list of shoes
          const data = await response.json();
            
          // Create a list of for all the requests and
          // add all of the requests to it
          const requests = [];
          for (let shoe of data.shoes) {
            const detailUrl = `http://localhost:8080/api/shoes/${shoe.id}`;
            requests.push(fetch(detailUrl));
          }
  
          // Wait for all of the requests to finish
          // simultaneously
          const responses = await Promise.all(requests);
          // Set up the "columns" to put the shoes
          // information into
          const shoesColumns = [[], [], []];
  
          // Loop over the shoes detail responses and add
          // each to to the proper "column" if the response is
          // ok
          let i = 0;
          for (const shoesResponse of responses) {
            if (shoesResponse.ok) {
              const details = await shoesResponse.json();
              shoesColumns[i].push(details);
              i = i + 1;
              if (i > 2) {
                i = 0;
              }
            } else {
              console.error(shoesResponse);
            }
          }
  
          // Set the state to the new list of three lists of
          // shoes
          this.setState({shoesColumns: shoesColumns});
        }
      } catch (e) {
        console.error("error", e);
      }
    }
  
    render() {
      return (
        <>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add a pair of shoes</Link>
        </div>
        <div className="container">
            <h2>Current shoes</h2>
            <div className="row">
                {this.state.shoesColumns.map((shoesList, index) => {
                return (
                    <ShoesColumn key={index} list={shoesList} />
                );
                })}
            </div>
        </div>
        </>
      );
    }
  }
  
  export default ShoesList;