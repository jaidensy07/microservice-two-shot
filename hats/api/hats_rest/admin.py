from django.contrib import admin
from .models import LocationVO
# Register your models here.
class LocationVOAdmin(admin.ModelAdmin):
    pass

admin.site.register(LocationVO, LocationVOAdmin)